package demo.dto;

public class ErrorResponse {

	private String error;

	public ErrorResponse(String errorMessage) {
		super();
		this.error = errorMessage;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
