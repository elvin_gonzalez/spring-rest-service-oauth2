package demo.repositories;

import org.springframework.data.repository.CrudRepository;

import demo.dto.user.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByLogin(String login);
}
