package demo.handler;

import demo.dto.ErrorResponse;
import demo.exception.ResourceNotFoundException;

/*@ControllerAdvice*/
public class GlobalExceptionHandler {

/*	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
*/	public ErrorResponse handleException(ResourceNotFoundException e){
		return new ErrorResponse(e.getMessage());
	}
	
}
