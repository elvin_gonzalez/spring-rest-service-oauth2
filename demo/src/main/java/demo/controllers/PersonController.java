package demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.dto.Person;
import demo.exception.ResourceNotFoundException;
import demo.repositories.PersonRepository;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class PersonController {

	@Autowired
	private PersonRepository personRepository;

	@RequestMapping(method = RequestMethod.POST, value = "/person")
	public Person create(@RequestBody(required = true) Person person) {
		return personRepository.save(person);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/person/{id}")
	public Person update(@PathVariable("id") long id, @RequestBody(required = true) Person person) {
		person.setId(id);
		return personRepository.save(person);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/person/{id}")
	public void delete(@PathVariable("id") long id) {
		personRepository.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/person")
	public Iterable<Person> getAll() {
		return personRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/person/{id}")
	public Person getById(@PathVariable("id") long id) {
		Person person = personRepository.findOne(id);
		if (person == null) {
			throw new ResourceNotFoundException("The resource does not exists");
		}
		return person;
	}

}
