package demo.repositories;

import org.springframework.data.repository.CrudRepository;

import demo.dto.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
