# **Spring Boot Oauth2** #

A simple spring boot project secured with oauth2 bearer and refresh tokens.

### Dependencies ###

* Java 8 (Java 7 also works, you just need to change the java.version in the pom.xml)
* Maven 3.
* An application server with support for JPA 2.1 (Tomcat or WildFly will do). You can also chance the packaging to jar in the pom.xml and run it as an standalone application.
* A database to persist the data. I used H2 since it's easy to set up for a demo project like this.

### Setting up the project ###

To run this project is necessary to set up a database since the the users, clients, tokens and business entities need to be persisted. The file src/main/resource/import.sql is a dump of my test H2 database, so you can use it to create your own copy or setup a similar schema in your favorite RDBMS.

After creating your database you need to configure it in the src/main/resources/application.properties file so the application can access it. You can set the access directly:
~~~~
spring.datasource.url = jdbc:h2:file:C:/testdb
spring.datasource.username= sa
spring.datasource.password= sa
~~~~

Or through JNDI if your are deploying the application in a managed container:
~~~~
spring.datasource.jndi-name=java:jboss/datasources/h2Person
~~~~

### Building and running the project ###

Since the project is built with maven, you can simply run **mvn clean package spring-boot:run** to build it and run it as an stand-alone application. 

Alternatively you can also **mvn clean package** to only build the project. The produced .war file will be located in the ./target directory so you can deploy it in your preferred application server as long it supports JPA 2.1 specification (or you could use one that does not support the JPA at all, like tomcat, and let spring manage it for you).

## **Hands-on** ##

The application exposes the following endpoints:

* /person Can be used to apply CRUD operations to the Persons (our business entity). Any authenticated user can access it this resource.
* /users Can be used to GET the users of the application. Only admin users can access it this resource.
* /oauth/token Used to authenticate our user and get or refresh the tokens needed to request the application's resources.

### Getting a token ###

If you try to access any of our resources your request will be rejected with an 401 Unauthorized error:
~~~~
> curl -X GET http://localhost:8080/person -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /person HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
>
< HTTP/1.1 401 Unauthorized
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< WWW-Authenticate: Bearer realm="restservice", error="unauthorized", error_description="Full authentication is required to access this resource"
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:06:44 GMT
<
{"error":"unauthorized","error_description":"Full authentication is required to access this resource"}
~~~~

To successfully request a resource you'll need to send an access token, to get this token you have to send a request to the /oauth/token endpoint with the authentication details of both the application client (using http basic authentication) and the user (POSTed as x-www-form-urlencoded).

~~~~
> curl -X POST -H "Accept: application/json" -u clientapp:123456 -H "Content-Type: application/x-www-form-urlencoded" -d "password=pepe&username=pepe&grant_type=password&scope=read+write" http://localhost:8080/oauth/token -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
* Server auth using Basic with user 'clientapp'
> POST /oauth/token HTTP/1.1
> Host: localhost:8080
> Authorization: Basic Y2xpZW50YXBwOjEyMzQ1Ng==
> User-Agent: curl/7.43.0
> Accept: application/json
> Content-Type: application/x-www-form-urlencoded
> Content-Length: 64
>
* upload completely sent off: 64 out of 64 bytes
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:21:13 GMT
<
{"access_token":"c1a90c21-19f3-4786-aea8-33692cd07274","token_type":"bearer","refresh_token":"69bdfc42-2122-4f45-9553-2bcb1d8fcb8e","expires_in":43199,"scope":"read write"}
~~~~

Now we have an access token and we can use it to request our protected resources.

### Using a token to access a protected resource ###

Once you got an access token you can access our resources send this token in the authorization header of the request, you also need to specify that it is a bearer token so the header will look like "Authorization: Bearer access-token-value":

~~~~
> curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer c1a90c21-19f3-4786-aea8-33692cd07274" http://localhost:8080/person -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /person HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
> Content-Type: application/json
> Authorization: Bearer c1a90c21-19f3-4786-aea8-33692cd07274
>
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:24:21 GMT
<
[{"id":1,"name":"Elvin","lastName":"Gonzalez"},{"id":2,"name":"Antonio","lastName":"Gonzalez"}]
~~~~

As you can see, we now got an HTTP 200 code (SUCCESS) and the response body contains the JSON encoded resource we requested for: an array with all persons in the application.

### Accessing resources by user role ###

We have seen that we can access protected resources using access tokens, however we can only access some resources based on the Roles the user have. The user we tried previoulsy only has the USER_ROLE, so if we try to access a resource restricted to administrators only, we'll get an HTTP 300 FORBIDDEN error:

~~~~
> curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer c1a90c21-19f3-4786-aea8-33692cd07274" http://localhost:8080/users -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /users HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
> Content-Type: application/json
> Authorization: Bearer c1a90c21-19f3-4786-aea8-33692cd07274
>
< HTTP/1.1 403 Forbidden
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:37:03 GMT
<
{"error":"access_denied","error_description":"Access is denied"}
~~~~

Let's get a token for a admin user:

~~~~
> curl -X POST -H "Accept: application/json" -u clientapp:123456 -H "Content-Type: application/x-www-form-urlencoded" -d "password=elvin&username=elvin&grant_type=password&scope=read+write" http://localhost:8080/oauth/token -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
* Server auth using Basic with user 'clientapp'
> POST /oauth/token HTTP/1.1
> Host: localhost:8080
> Authorization: Basic Y2xpZW50YXBwOjEyMzQ1Ng==
> User-Agent: curl/7.43.0
> Accept: application/json
> Content-Type: application/x-www-form-urlencoded
> Content-Length: 66
>
* upload completely sent off: 66 out of 66 bytes
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:39:37 GMT
<
{"access_token":"ad7e9d21-6d05-4c1f-b34d-7ed805cfcc1e","token_type":"bearer","refresh_token":"97122ed6-c285-4241-84b9-18297153df83","expires_in":41772,"scope":"read write"}
~~~~

Since this user has the ADMIN_ROLE we can now access our restricted resource:

~~~~
> curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer ad7e9d21-6d05-4c1f-b34d-7ed805cfcc1e" http://localhost:8080/users -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /users HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
> Content-Type: application/json
> Authorization: Bearer ad7e9d21-6d05-4c1f-b34d-7ed805cfcc1e
>
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:40:48 GMT
<
[{"id":1,"name":"Elvin","login":"elvin","password":"elvin"},{"id":2,"name":"Pepe","login":"pepe","password":"pepe"},{"id":3,"name":"Loco","login":"loco","password":"loco"}]
~~~~

And there it is, we can see our protected resource, that is, all the sensible information of the users of our application, including their username and passwords. 

As we can see we can use oauth2 to not only secure our resources/services so they can only be accessed by trusted applications, but we can also put in place a role-based access control.

### Refreshing a token ###

The access token have a lifetime and once they expire we must ask for a new one; if we try to use the expired token well get again an HTTP 401 error:

~~~~
> curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer a7c3a0fa-161a-450c-adee-b8e4b6eccb97" http://localhost:8080/users -v
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /users HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.43.0
> Accept: */*
> Content-Type: application/json
> Authorization: Bearer a7c3a0fa-161a-450c-adee-b8e4b6eccb97
>
< HTTP/1.1 401 Unauthorized
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< WWW-Authenticate: Bearer realm="restservice", error="invalid_token", error_description="Invalid access token: a7c3a0fa-161a-450c-adee-b8e4b6eccb97"
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:50:04 GMT
<
{"error":"invalid_token","error_description":"Invalid access token: a7c3a0fa-161a-450c-adee-b8e4b6eccb97"}
~~~~

We could of course pass again through the authentication process using the username and password, but for security reasons we want to minimize the number of times the authentication details are send over network, so we can use instead the refresh-token that we obtained along with our previous access token, so we request again our /oauth/token endpoint, but this time we use the refresh token instead:

~~~~
> curl -X POST -vu clientapp:123456 http://localhost:8080/oauth/token -H "Accept: application/json" -d "grant_type=refresh_token&refresh_token=97122ed6-c285-4241-84b9-18297153df83"
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
* Server auth using Basic with user 'clientapp'
> POST /oauth/token HTTP/1.1
> Host: localhost:8080
> Authorization: Basic Y2xpZW50YXBwOjEyMzQ1Ng==
> User-Agent: curl/7.43.0
> Accept: application/json
> Content-Length: 75
> Content-Type: application/x-www-form-urlencoded
>
* upload completely sent off: 75 out of 75 bytes
< HTTP/1.1 200 OK
< Server: Apache-Coyote/1.1
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Cache-Control: no-cache, no-store, max-age=0, must-revalidate
< Pragma: no-cache
< Expires: 0
< X-Frame-Options: DENY
< Cache-Control: no-store
< Pragma: no-cache
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Tue, 07 Jul 2015 16:56:11 GMT
<
{"access_token":"dd20707c-9ddf-4751-a5e5-a6d3fcf72b3b","token_type":"bearer","refresh_token":"97122ed6-c285-4241-84b9-18297153df83","expires_in":43199,"scope":"read write"}
~~~~

And now we got a new access token we can use to request our resources, notice that the refresh token stays the same, this is because the refresh token also expire, but they last longer than an access token, this way we only need to send over our user authentication details once the refresh token has expired (or has been invalidated by an admin for security reason).